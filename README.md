# Installing a Virtual Private Development Server (v3.17b)

This document will guide you through installing a Virtual Private Server (VPS) for development purposes.

The VPS installed going through this guide serves as a reference platform for several courses on the PBA for Web Development at Copenhagen School of Design and Technology (KEA, Københavns Erhvervsakademi).

We use Linode as an infrastructure-as-a-service provider for this guide, and while Linode services are not free, they are very affordable.
Most of this guide would work very similarly with other service providers, but we use Linode as a reference platform and will not consider other platforms.

We suggest you fork this repository into a private repository on GitLab and make notes about the installation as you go through it.
You are welcome to suggest improvements to the guide via a merge request.

A few installation procedures differ between Linux, macOS, and Windows.
While we do our best to make this work on all platforms, things sometimes change, so a few things might look different from what they do in this guide.
Use your common sense and problem-solving skills to work through such issues.

> You might want to use your mobile phone's internet connection during this setup.
    Linode have certain measures in place to protect from people abusing their systems, and these measures might get triggered if a very large number of students creates accounts from the same (KEA's) IP address.
    So, if possible, you can use your own internet connection for setting up the development server.
    Very little data will be used for this purpose.


## Creating SSH Keys

The first thing we need to do is to create an SSH public/private key pair.
When using SSH with public/private keys, we get a very high level of security, and we do not have to use passwords if we do not want to.

**MacOS and Linux:**
If your computer is running macOS or Linux, check if you already have SSH keys on your computer by running this command in a terminal:

```bash
ls ~/.ssh
```

If you get a list of files `id_rsa` and `id_rsa.pub` you already have keys on your system.
These are your **private** and **public** keys, respectively.
Your keys might have a slightly different name, such as `id_ed25519` and `id_es25519.pub`, which just means that your keys use a different encryption algorithm, which is not a problem.

If you do not have any keys listed, create a new public/private key-pair using this command:

```bash
ssh-keygen -t ed25519 -a 100
```

You must answer a few questions, but you can leave everything blank or default by simply pressing RETURN on every question.

> You can leave the password blank by simply pressing RETURN when asked for password and password confirmation. Doing so means you don't have to enter any passwords when working with SSH. It is safe to do so as long as you keep your private key safe and private.

**Windows:**
If your computer is running Windows, please follow this guide to create a public/private key-pair.
We recommend that you use the first method described (OpenSSH).

[https://phoenixnap.com/kb/generate-ssh-key-windows-10/](https://phoenixnap.com/kb/generate-ssh-key-windows-10/)


## Create A Linode Account

You need a Linode account to set up a VPS with Linode.
On the link below, you will receive a $100, 60-day credit when finishing the registration:

[https://www.linode.com/?r=1cc5604a5a6964df89a68075a4919e770101e7e1](https://www.linode.com/?r=1cc5604a5a6964df89a68075a4919e770101e7e1)

**DISCLAIMER:** The link above is my referral link - I will also receive some credit from you signing up via this link.
You are welcome to sign up some other way if you prefer.

We now need to upload your **public** key to Linode:

- Once logged in to your account, click your user name in the top right corner, and select *SSH Keys* under *My Profile*.
- Click the *Add an SSH Key* button.
- Supply a label (e.g., the name of your user account and machine: *henrik@minime*).
- Open the public file called e.g. ed25519.pub
- Paste the **contents** of your **public** key in the *SSH Public Key* field.

> The **contents** of your public key should look something like `ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIK0wmN/Cr3JXqmLW7u+g9pTh+wyqDHpSQEIQczXkVx9q gleb@reys.net`

We would also recommend that you enable two-factor authentication under *Login & Authentication*, but that is up to you.


## Create A Linode Virtual Private Server

In the left column menu, choose *Linodes*.

Click *Create Linode*, and set the following options:

- Choose a Distribution:
  - Images: Alpine 3.17
- Region: Europe, Frankfurt, DE
- Linode Plan: Under *Shared CPU* select Nanode 1GB, $5 monthly, 1GB RAM, 1 CPU, 25GB storage
- Linode Label: kea-development-server
- Root Password: *set a good, long password - keep it safe!*
- SSH Keys: *check the box next to your user name to include your SSH keys*
- Optional Add-ons - Backup: *check this if you want Linode to back up your VPS - this is recommended*

Your plan should come to $5 or $7 a month, depending on whether you selected backups or not.

Click *Create Linode* to create your new VPS.

Your VPS will now be created and started, which usually takes a few minutes.

Make a note of the *IP Address* in the top right corner: the first one is the IPv4 address, e.g., `139.162.141.12` in my case; you will need this address to reach your VPS (your IP address will be different).


## Add Your VPS To Your SSH Configuration

To make it easy to access the `kea-dev` server, we will add a configuration for it to the SSH configuration file.

If you have `vim` installed on your local computer, you can use the command:

```bash
vim ~/.ssh/config
```

Make sure the file looks like this, replacing the IP address with your VPS IP address:

```bash
Host kea-dev
    HostName 139.162.141.12
    ServerAliveInterval 20
    TCPKeepAlive no
    LocalForward 8000 127.0.0.1:8000
    LocalForward 4000 127.0.0.1:4000
```

Save the file and exit.

> If you are on a Windows computer, `vim` is probably not installed on your computer.
> You can instead use a program like `Notepad`.\
> Please notice, that `Notepad` will likely add the `.txt` extension to your file.
> You will have to remove it using the command: `ren config.txt config` inside your `.ssh` directory.


## Connect to your Virtual Private Server

Now we can connect to the VPS using SSH - open a terminal or command window and type this command, and answer **'yes'** to continue:

```bash or zsh
ssh root@kea-dev
```

> If you are reinstalling your development server you will get an error message because the SSH fingerprint of your old server does not match the fingerprint of your new server. Help help this problem, you can simply delete the file `.ssh/known_hosts` on your local computer (doing this will prompt you to accept the fingerprint of other services like GitLab). Alternatively, you can edit the file and remove line entries for the development server (starting with `kea-dev` or your server's IP address).

```text
The authenticity of host kea-dev (139.162.141.12) can\'t be established.
ECDSA key fingerprint is SHA256:l4X3SRo3eHLVzDIbT4HF1lCQaMhzcyFI5Cjkxl9etMQ.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'kea-dev,139.162.141.12' (ECDSA) to the list of known hosts.
Welcome to Alpine!

The Alpine Wiki contains a large amount of how-to guides and general
information about administrating Alpine systems.
See <http://wiki.alpinelinux.org/>.

You can setup the system with the command: setup-alpine

You may change this message by editing /etc/motd.

localhost:~#
```

> All the commands you enter when logged in to your VPS will run on the VPS - not your local computer.


## Run Config Script

We will configre our new virtual private server using a configuration script.
The script, and some files use by the script, are contained in a `git` repository.

To get this repository we must first install `git`:

```bash
apk add git
```

Next, we can clone the repository:

```bash
git clone https://gitlab.com/henrikstroem/vps-reference-config.git
```

> The repository contains a number of configuration files that will help you set up your VPS.

The script needs to know your username.

> Substitute the name `henrik` below with the username on your local computer. It will be easiest if you use the same name locally and on the KEA development server. If you don't, you must specify the username like we did with `root` above. That is not necessary if your local and remote usernames are the same.

```bash
export KEAUSER=henrik
```

Now we are ready to run the installation script:

```bach
./vps-reference-config/install/mk-kea-dev
```

> You are now going to set a password. Make sure to choose a secure password and write it down. Note: Try to avoid characters like `!#` in your password. The characters may give you a problem loging in, and locked you out. If so, rebuild the vps again. You may just want to use the suggested password.

The script should end with a message telling you to activate your user.

> Make sure to make a note of your password. You might get completely locked out of your KEA Development Server if you lose this password. Also, keep it safe!

Do that, and reboot the server as instructed.

Your KEA Development Server will then be ready after the reboot, which usually takes two or three minutes.


## Improve DX With Dotfiles

The installation is a bit rough out-of-the-box.
You can improve your developer experience by adding a few configuration files for the shell and the `vim` editor.

> You should only perform these steps after the reboot instructed above.

First we must clone the repository once more for your new user:

```bash
git clone https://gitlab.com/henrikstroem/vps-reference-config.git
```

Next, we must unzip the configuration files:

```bash
unzip vps-reference-config/install/dotfiles.zip
```

Now exit from `kea-dev` and re-connect to have the configuration take effect.

Once you are reconnected, run `vim`:

```bash
vim
```

Press `ENTER` to acknowledge the error, then wait for the installs to run and press `ENTER` again.

Press `ESC` to make sure you are in `NORMAL` mode, and run the command `:PlugInstall`.

You can now close the plug-installer pressing `ESC` and `:bd`, and quit `vim` pressing `ESC` and `:q`.

Your `kea-dev` server is now ready.
